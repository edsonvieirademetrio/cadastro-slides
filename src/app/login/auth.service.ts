import { Usuario } from './usuario';
import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private usuarioAutenticado:boolean = false

  mostrarMenuEmitter = new EventEmitter<boolean>()
  msgNaoAutenticado:string

  constructor(private router: Router) { }

  fazerLogin(usuario:Usuario){

    if(usuario.nome === 'ed@ed.com' && usuario.senha === '123'){
      this.usuarioAutenticado = true
      this.mostrarMenuEmitter.emit(true)
      this.router.navigate(['/'])
    }else{      
      this.usuarioAutenticado = false      
      this.mostrarMenuEmitter.emit(false) 
      this.msgNaoAutenticado = 'Usuário ou Senha Inválidos.'
      alert(this.msgNaoAutenticado)
      location.reload()

    }

  }
  usuarioEstaAutenticado(){
    return this.usuarioAutenticado
  }
}
