import { LoginComponent } from './login/login.component';
import { Routes } from '@angular/router'
import { AppComponent } from './app.component'
import { ListComponent } from './contatos/list/list.component'
import { EditComponent } from './contatos/edit/edit.component'
import { ListDetailComponent} from './list-detail/list-detail.component'
import { AuthGuard } from './guards/auth.guard';

export const ROUTES: Routes = [
    {path: '', component: EditComponent, canActivate:[AuthGuard]},
    {path: 'list', component: ListComponent},
    {path: 'list/:key', component: ListDetailComponent},
    {path: 'login', component: LoginComponent}

]