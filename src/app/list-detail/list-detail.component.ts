import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { ContatoDataService } from '../contatos/shared/contato-data.service';
import { ContatoService } from '../contatos/shared/contato.service';
import { ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.sass']
})
export class ListDetailComponent implements OnInit {

  contatos: Observable<any> 

  constructor(private contatoService: ContatoService, private contatoDataService: ContatoDataService, private route: ActivatedRoute) { }

  ngOnInit() {
    let key = this.route.snapshot.params['key']
    this.contatos = this.contatoService.getByKey(key)    
  }

}
